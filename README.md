# Project

## Objective

You'd like to produce a simple graph showing the in/out flows of Covid-hospitalized patients, in France.  
To do so, you plan to replicate this graph:

![Graph](https://raw.githubusercontent.com/rozierguillaume/covid-19/master/images/charts/france/hosp_journ_flux.jpeg)

In principle, this is quite straightforward as data is already accessible from public API.  
Everything could be rendered in a single step on the frontend.

Yet, this project includes some constraints.

## Constraints

- Data is grabbed from public sources
- Stored in a server, to ensure robustness if something brakes on the source side
- An API is built
- Data is accessed through the API, from the frontend

## What is evaluated

You may not have enough time to properly finish this project. This is normal.

### What is crucial:

1. **Overview** - how you plan to tackle this challenge.
2. **Defend your choices** - why you decided to use those approaches taking into account their advantages, drawbacks and how they fit with the objective.
3. **Do what you can** - implement as fast and reliable as you can, doesn't matter much if it's not finished.

### What is important:

1. **Code quality** - how clean and robust is the stack.
2. **Reproducibility** - how easy it is to deploy what you built.
3. **Robustness** - how errors are handled, what happens if a service breaks.

### Those are bonuses:

1. **Dynamic** - dynamic graph on client (svg rather than raster image & zoomable)
2. **Autonomous** - automatically ingest new data from the source, to the API

## Tips

A bunch of technologies and approaches we are used to, it should guide you a bit. Feel free to make your own choices !

- REST / GraphQL
- Typescript
- Node
- Bash
- Containers
- PostgreSQL
- Microservices
- Python
- JSON

## Final notes

This is merely an exercise and we'd rather you come up with a solution - without implementing it properly - than figuring things out as you go.

If you get questions: nedelec@wedata.science

Cheers,